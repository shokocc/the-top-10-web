import ListPLayersAdmin from "@/components/ListPLayersAdmin.vue";

describe("ListPLayersAdmin", () => {
  it("send title", () => {
    expect(typeof ListPLayersAdmin.created).toBe("function");
    const data = ListPLayersAdmin.data("test");
    expect(data.title).toBe();
  });
});
