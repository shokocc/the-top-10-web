import ListPlayers from "@/components/ListPlayers.vue";

describe("ListPlayers", () => {
  it("send title", () => {
    expect(typeof ListPlayers.created).toBe("function");
    const data = ListPlayers.data("test");
    expect(data.title).toBe();
  });
});
