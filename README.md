# the-top-w

### Environment
.env Is used for local commands
```
VUE_APP_API_BACK=http://localhost:3000/api/
```

.env.dev Is used for docker and ci/cd flow
```
VUE_APP_API_BACK=https:/latgames.net/api/
```

## Project setup
```
docker-compose build
```

### Run project
```
docker-compose up
```

### Run your unit tests
```
docker-compose run --rm web npm run test:unit
```

### Lints and fixes files
```
docker-compose run --rm web npm run lint
```
