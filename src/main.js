import Vue from "vue";
import app from "./app.vue";
import router from "./router";
import store from "./store";
import vuetify from "./plugins/vuetify";
import axios from "axios";
import VueAxios from "vue-axios";

Vue.config.productionTip = false;

new Vue({
  router,
  store,
  vuetify,
  VueAxios,
  axios,
  render: (h) => h(app),
}).$mount("#app");