import Api from './api';
// import { Player } from "../interfaces/player.interfaces";
// import { User } from "../interfaces/user.interfaces";

class UserService {
  getUsers(options = {}) {
    let query = '';

    if (options.take) query += `&take=${options.take || 10}`;
    if (options.skip) query += `&skip=${options.skip || 0}`;
    if (options.email) query += `&search[email]=${encodeURIComponent(options.email)}`;
    if (options.nickname) query += `&search[nickname]=${encodeURIComponent(options.nickname)}`;
    if (options.status) query += `&search[status]=${encodeURIComponent(options.status)}`;

    return Api(true).get('users?' + query);
  }
  // getUsers() {
  //   return Api(true).get('users?take=10&skip=0');
  // }

  getUser(id) {
    return Api(true).get(`users/${id}`);
  }

  postUser(user) {
    return Api(true).post('users', { user: user });
  }

  putUser(user) {
    return Api(true).put(`users`, { user: user });
  }

  deleteUser(id) {
    return Api(true).delete(`users/${id}`);
  }
}

export const userService = new UserService();
