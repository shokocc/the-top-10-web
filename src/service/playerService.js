import Api from "./api";

class PlayerService {
  
  //obtiene el token
  getToken(){
    return localStorage.access_token
  }
  

  getPlayers(key,value) {
    const params={
      key:key,
      value:value
    }
    return Api().get("players",params);
  }

  getPlayer(id) {
    return Api().get(`players/${id}`);
  }

  postPLayer(player, user) {
    return Api().post("auth/register",{player:player,user:user});
  }

  putPlayer(player) {
    return Api(true).put("players",{ player :player});
  }

  deletePlayer(id) {

    return Api(true).delete(`${id}`);
  }
}

export const playerService = new PlayerService();