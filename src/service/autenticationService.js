import Api from './api';
// import { User } from "../interfaces/user.interfaces";

class AutenticationService {
    login(email,password){
        return Api().post('auth/login',{email,password})
    }

    register(nickname, ranking, avatar_url ,email,password){
        return Api().post('auth/register',{
            "player": {
                "nickname": nickname,
                "ranking": ranking,
                "avatar_url": avatar_url
            },
            "user": {
                "email": email,
                "password": password
            }
        })
    }
}

export const autenticationService = new AutenticationService();