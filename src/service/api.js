import axios from "axios";

export default (useToken = false) => {
  const headers = {
    Accept: "application/json",
    "Content-type": "application/json",
  };

  if(useToken && localStorage.access_token)
    headers['Authorization'] = 'Bearer ' + localStorage.access_token;

  return axios.create({
    baseURL: process.env.VUE_APP_API_BACK,
    headers,
  });
};
