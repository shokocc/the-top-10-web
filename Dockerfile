FROM node:16 AS builder

USER root

# Set up the build environment
ENV NPM_CONFIG_PREFIX=/home/node/.npm-global
ENV PATH=$PATH:/home/node/.npm-global/bin

# Create app directory
WORKDIR /home/vue

# Copy app files
COPY package.json .
COPY . .

# Install dependencies
RUN npm install